import React, {createContext, useReducer} from 'react';
import {authReducer} from './authReducer';

// Definir como luce, que informacion tendremos
export interface AuthState {
  isLoggedIn: boolean;
  username?: string;
  favoriteIcon?: string;
}

// estado inicial
export const authInitialState: AuthState = {
  isLoggedIn: false,
  username: undefined,
  favoriteIcon: undefined,
};

// lo usaresmo para decirle a React como luce y que expone el context
export interface AuthContextProps {
  authState: AuthState;
  signIn: () => void;
  logout: () => void;
  changeFavoriteIcon: (iconName: string) => void;
  changeUsername: (username: string) => void;
}

// Crear el contexto
export const AuthContext = createContext({} as AuthContextProps);

// compoenente proveedor del estado
export const AuthProvider = ({children}: any) => {
  // el 'dispatch' es el unico medio para hacer una modificacion al estado
  const [authState, dispatch] = useReducer(authReducer, authInitialState);

  const signIn = () => {
    // dispatch de la accion
    dispatch({type: 'signIn'});
  };

  const changeFavoriteIcon = (iconName: string) => {
    dispatch({type: 'changeFavIcon', payload: iconName});
  };

  const logout = () => {
    dispatch({type: 'logout'});
  };

  const changeUsername = (username: string) => {
    dispatch({type: 'changeUsername', payload: username});
  };

  return (
    <AuthContext.Provider
      value={{authState, signIn, logout, changeFavoriteIcon, changeUsername}}>
      {children}
    </AuthContext.Provider>
  );
};
