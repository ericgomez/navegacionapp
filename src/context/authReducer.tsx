import {AuthState} from './AuthContext';

// Creando la accion
type AuthActions =
  | {
      type: 'signIn';
    }
  | {
      type: 'logout';
    }
  | {
      type: 'changeFavIcon';
      payload: string;
    }
  | {
      type: 'changeUsername';
      payload: string;
    };

export const authReducer = (
  state: AuthState,
  action: AuthActions, // Recive la accion
): AuthState => {
  switch (action.type) {
    case 'signIn': // caso de la accion
      // Regresamos un nuevo estado
      return {
        ...state,
        isLoggedIn: true,
        username: 'no-username-yet',
      };
    case 'logout': // caso de la accion
      // Regresamos un nuevo estado
      return {
        ...state,
        isLoggedIn: false,
        username: undefined,
        favoriteIcon: undefined,
      };
    case 'changeFavIcon': // caso de la accion
      // Regresamos un nuevo estado
      return {
        ...state,
        favoriteIcon: action.payload,
      };
    case 'changeUsername': // caso de la accion
      // Regresamos un nuevo estado
      return {
        ...state,
        username: action.payload,
      };

    default:
      return state;
  }
};
