import React, {useEffect} from 'react';
// import {StackScreenProps} from '@react-navigation/stack';
import {DrawerScreenProps} from '@react-navigation/drawer';
import {Button, Text, View, TouchableOpacity} from 'react-native';
import {styles, colors} from '../theme/appTheme';
import Icon from 'react-native-vector-icons/Ionicons';

// interface Props extends StackScreenProps<any, any> {}
interface Props extends DrawerScreenProps<any, any> {}

export const Page1Screen = ({navigation}: Props) => {
  useEffect(() => {
    navigation.setOptions({
      headerLeft: () => (
        <TouchableOpacity
          style={{marginLeft: 10}}
          onPress={() => navigation.toggleDrawer()} // Open toggleDrawer
        >
          <Icon
            name="menu-outline"
            size={20}
            color={colors.primary}
            size={35}
          />
        </TouchableOpacity>
      ),
    });
  }, []);

  return (
    <View style={styles.globalMargin}>
      <Text style={styles.title}>Page1Screen</Text>
      <Button
        title="go to page 2"
        onPress={() => navigation.navigate('Page2Screen')}
      />

      <Text style={{marginVertical: 20, fontSize: 20}}>Navigate with args</Text>

      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          style={{...styles.buttonBig, backgroundColor: '#5856D6'}}
          onPress={() =>
            navigation.navigate('PersonScreen', {
              id: 1,
              name: 'Eric',
            })
          }>
          <Icon name="person-outline" size={20} color="white" size={35} />
          <Text style={styles.buttonBigText}>Eric</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={{...styles.buttonBig, backgroundColor: '#FF9427'}}
          onPress={() =>
            navigation.navigate('PersonScreen', {
              id: 2,
              name: 'Maria',
            })
          }>
          <Icon name="person-outline" size={20} color="white" size={35} />
          <Text style={styles.buttonBigText}>Maria</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
